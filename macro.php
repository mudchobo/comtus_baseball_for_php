<?php

$osInfo = strtoupper(substr(PHP_OS, 0, 3));

if (count($argv) <= 2)
{
    echo 'argv enter. mode subMode device deviceip ex) league match genymotion (101)' . PHP_EOL;
    exit;
}
$mode = $argv[1];
$subMode = $argv[2];
$device = $argv[3];
$data = json_decode(file_get_contents("$device.json"), true);
if ($data == null) {
    echo 'Error!' . PHP_EOL;
    exit;
}
$deviceName = $data['deviceName'];
if ($device == 'genymotion') {
    $deviceIp = (isset($argv[4]) && $argv[4] != '') ? $argv[4] : '101';
    $deviceName = "192.168.56.{$deviceIp}:5555";
} else {
    $deviceIp = $device;
}
echo "mode = $mode, deviceName = $deviceName" . PHP_EOL;

// 최초 실행하기 위함.
$isFirst = true;
// 버튼이 클릭이 안될 때 재시작하기 위한 카운트.
$stopCount = 0;
$result = false;
$matchResult = false;
// 시합중인지 체크.
$isMatch = false;

while(true)
{
    // 최초 genymotion 실행.
    if ($isFirst)
    {
        if ($device == 'genymotion') {
        }
        exec("adb -s {$deviceName} shell am start -n com.com2us.probaseball3d.normal.freefull.google.global.android.common/com.com2us.probaseball3d.normal.freefull.google.global.android.common.MainActivity");
        $isFirst = false;
        $stopCount = 0;
    }
    // screen capture
    echo "screen capture..." . PHP_EOL;
    if ($osInfo == 'DAR') {
        system("adb -s {$deviceName} shell screencap -p | perl -pe 's/\\x0D\\x0A/\\x0A/g' > screen_{$deviceIp}.png");
    }
    else if ($osInfo == 'WIN') {
        system("adb -s {$deviceName} shell screencap -p /sdcard/screen_{$deviceIp}.png");
        system("adb -s {$deviceName} pull /sdcard/screen_{$deviceIp}.png");
        system("adb -s {$deviceName} shell rm /sdcard/screen_{$deviceIp}.png");
    }
    $img = imagecreatefrompng("screen_{$deviceIp}.png");
    echo 'mode = ' . $mode . PHP_EOL;

    checkSpecifyKey('touchScreen1');
    checkSpecifyKey('touchScreen2');
    checkKey('popupAd');
    checkKey('attend');
    checkKey('monthly');
    checkKey('reviewNo');
    checkKey('back500');
    checkKey('isExitNo');

    // 리그 시 오른쪽 하단 계속 클릭.
    if ($mode == 'l_a') {
        checkKey('leagueMode');
        checkKey('leagueStart1');
        checkKey('continue');
        checkKey('onlyDefenseCheck');
        checkKey('upgradeX');

        // 혹시 선수관리 페이지로 잘못 들어온 경우
        checkSpecifyKey('isManagePlayer');
        // 혹시 친구초대 페이지로 잘못 들어온 경우
        checkSpecifyKey('isInviteFriend');
        // 라인업 스킵
        if (checkKey('startingLineUpSkip')) {
            sleep(1);
            clickKey('startingLineUpSkip');
        }
        // 리그 완료 후 다음버튼
        if (checkKey('completeNext')) {
            usleep(500000);
            clickKey('completeNext');
        }
        checkKey('completeConfirm1');
        checkKey('completeConfirm2');
        checkKey('retryLeague');
        if (checkKey('selectFullLeague')) {
            clickKey('nextAfterSelect');
        }
        if (checkPixelKey('selectGoal')) {
            clickKey('selectGoal1');
            sleep(1);
            clickKey('selectGoal2');
            sleep(1);
            clickKey('selectGoal3');
            sleep(1);
            clickKey('nextAfterSelect');
            sleep(1);
            clickKey('nextAfterSelect');
        }

        // 현재 모드가 자동이 아닌 경우 자동버튼 체크
        if ($subMode != 'auto' && checkSpecifyKey('checkAuto')) {
            // 버그인지 한 번 더 클릭해줘야 함.
            sleep(1);
            clickEvent(85, 347);
            $subMode = 'auto';
        }
        checkKey('leagueStart2');

        // 현재 모드가 시합중이 아닌 경우 시합인 지 체크
        if ($subMode != 'match' && checkPixelKey('isMatch')) {
            echo "match!" . PHP_EOL;
            $subMode = 'match';
        }

        // 자동 시합인 경우
        if ($subMode == 'auto') {
            for ($i = 0; $i < 5; $i++) {
                clickEvent(88, 1720);
            }
        }
        // 실제 시합인 경우
        else if ($subMode == 'match') {
            // 대기타선 화면일 때 클릭
            checkKey('readySkip');
            checkKey('infoSkip');
            checkKey('pitcherSkip');
            checkKey('fastClick');
            checkKey('matchStart');

            // 경기 끝나고 다음.
            checkKey('endNext');
            checkKey('cheerBonus');
            checkKey('cheerConfirm');
            checkKey('endConfirm');

            // 체력이 멀쩡한 지 체크 후 멀쩡하면 구종선택 및 던지기
            $matchResult = checkPixelKey('isEnoughHealth');
            if ($matchResult) {
                // 구종 수 체크.
                $count = 0;
                for ($i = 1; $i <= 5; $i++) {
                    if (checkPixelKey("ball$i")) {
                        $count++;
                    }
                }
                $randValue = rand(1, $count);
                clickKey("ball$randValue");
                echo "count = $count, randValue = $randValue" . PHP_EOL;

                usleep(500000);
                // 위치는 구석구석 랜덤.
                $randValue = rand(1, 9);
                clickKey("position$randValue");
            }
            // 체력이 빨간색인 경우 투수 교체.
            if (checkPixelKey('isLackHealth1') || checkPixelKey('isLackHealth2')) {
                clickKey('pause');
                usleep(500000);
                clickKey('changePlayer');
            }
            // 선수교체 화면인 경우
            if (checkPixelKey('isChangePlayer')) {
                // 100%인 선수가 있으면 클릭 후 교체하기.
                for ($i = 1; $i <= 5; $i++) {
                    if (checkPixelKey("isPitcher$i")) {
                        clickKey("isPitcher$i");
                        usleep(500000);
                        clickKey('changePitcher');
                        usleep(500000);
                        clickKey('changePitcher');
                        break;
                    }
                }
            }
        }
        $result = checkSpecifyKey('anotherResult');
    }
    // 실제 경기
    else if ($mode == 'l_m') {
        checkKey('leagueMode');
        checkKey('leagueStart1');
        checkKey('leagueStart2');
        checkKey('continue');

        // 어웨이인 경우 수비만.
        if (checkPixelKey('isAway')) {
            echo "원정!" . PHP_EOL;
            clickKey('onlyDefense');
        }
        // 홈인 경우 공격만.
        else if (checkPixelKey('isHome')) {
            echo "홈!" . PHP_EOL;
            clickKey('onlyAttack');
        }
        if (checkKey('autoContinue') || checkKey('inningAuto')) {
            // 9이닝까지 드래그
            sleep(2);
            dragKey('inningMax');
        }
        if (checkKey('inningConfirm') || checkKey('inningConfirm2')) {
            sleep(1);
            clickKey('fast');
        }
        $result = checkSpecifyKey('anotherResult');

        checkKey('fastClick');
        checkKey('endNext');
        checkKey('cheerBonus');
        checkKey('cheerConfirm');
        checkKey('endConfirm');
        checkKey('upgradeX');
    }
    // 친선 경기인 경우
    else if ($mode == 'match') {

    }

    if ($result == false && $matchResult == false) {
        $stopCount++;
        if ($stopCount == 300) {
            echo 'stop!!' . PHP_EOL;
            exec("adb -s {$deviceName} shell am force-stop com.com2us.probaseball3d.normal.freefull.google.global.android.common");
            sleep(10);
            $isFirst = true;
        }
    } else {
        $result = false;
        $matchResult = false;
        $stopCount = 0;
    }
    echo 'stopCount = ' . $stopCount . PHP_EOL;
    echo 'sleep 2 second...' . PHP_EOL;
    sleep(1);
}

function checkKey($key) {
    global $data;
    return checkClick($data[$key][0], $data[$key][1], $data[$key][2], $data[$key][3]);
}
function checkSpecifyKey($key) {
    global $data;
    return checkSpecifyClick($data[$key][0], $data[$key][1], $data[$key][2], $data[$key][3], $data[$key][4], $data[$key][5]);
}
function clickKey($key) {
    global $data;
    clickEvent($data[$key][0], $data[$key][1]);
}
function checkPixelKey($key) {
    global $data;
    return checkPixel($data[$key][0], $data[$key][1], $data[$key][2], $data[$key][3]);
}
function dragKey($key) {
    global $data, $device;
    if ($device == 'genymotion') {
        drag($data[$key][0], $data[$key][1], $data[$key][2], $data[$key][3]);
    } else if ($device == 'g2') {
        drag_g2($data[$key][0], $data[$key][1], $data[$key][2], $data[$key][3]);
    }

}

function checkClick($x, $y, $color, $ment = '')
{
    global $img;
    $result = checkPixel($x, $y, $color, $ment);
    if ($result)
    {
        clickEvent($x, $y);
        echo $ment . ' click!' . PHP_EOL;
    }
    return $result;
}
function checkSpecifyClick($x, $y, $color, $clickX, $clickY, $ment)
{
    global $img;
    $result = checkPixel($x, $y, $color, $ment);
    if ($result)
    {
        clickEvent($clickX, $clickY);
        echo $ment . ' specify click! x = ' . $clickX . ', y = ' . $clickY . PHP_EOL;
    }
    return $result;
}
function checkPixel($x, $y, $color, $ment)
{
    global $img;
    $rgb = imagecolorat($img, $x, $y);
    $r = ($rgb >> 16) & 0xFF;
    $g = ($rgb >> 8) & 0xFF;
    $b = $rgb & 0xFF;
    $a = ($rgb >> 24) & 0x7F;
    var_dump("$ment = $rgb, $r,$g,$b,$a");
    if ($rgb == $color)
    {
        return true;
    }
    return false;
}
function clickEvent($x, $y)
{
    global $deviceName, $device;
    $exec = "adb -s {$deviceName} shell \"";
    if ($device == 'genymotion') {
        $exec .= sendEvent('event7', 0x1, 0x14a, 0x1);
        $exec .= sendEvent('event7', 0x3, 0x3a, 0x1);
        $exec .= sendEvent('event7', 0x3, 0x35, $x);
        $exec .= sendEvent('event7', 0x3, 0x36, $y);
        $exec .= sendEvent('event7', 0x0, 0x2, 0x0);
        $exec .= sendEvent('event7', 0x0, 0x0, 0x0);
        $exec .= sendEvent('event7', 0x1, 0x14a, 0x0);
        $exec .= sendEvent('event7', 0x3, 0x3a, 0x0);
        $exec .= sendEvent('event7', 0x3, 0x35, $x);
        $exec .= sendEvent('event7', 0x3, 0x36, $y);
        $exec .= sendEvent('event7', 0x0, 0x2, 0x0);
        $exec .= sendEvent('event7', 0x0, 0x0, 0x0);
    } elseif ($device == 'g2') {
        $exec .= sendEvent('event0', 0x3, 0x39, 0xa1);
        $exec .= sendEvent('event0', 0x3, 0x35, $x);
        $exec .= sendEvent('event0', 0x3, 0x36, $y);
        $exec .= sendEvent('event0', 0x3, 0x30, 0xd);
        $exec .= sendEvent('event0', 0x3, 0x31, 0x9);
        $exec .= sendEvent('event0', 0x0, 0x0, 0x0);
        $exec .= sendEvent('event0', 0x3, 0x39, 0xffffffff);
        $exec .= sendEvent('event0', 0x0, 0x0, 0x0);
    } elseif ($device == 'galaxy3') {
        $exec .= sendEvent('event1', 0x3, 0x39, 0xa1);
        $exec .= sendEvent('event1', 0x3, 0x35, $x);
        $exec .= sendEvent('event1', 0x3, 0x36, $y);
        $exec .= sendEvent('event1', 0x3, 0x30, 0xd);
        $exec .= sendEvent('event1', 0x3, 0x31, 0x9);
        $exec .= sendEvent('event1', 0x0, 0x0, 0x0);
        $exec .= sendEvent('event1', 0x3, 0x39, 0xffffffff);
        $exec .= sendEvent('event1', 0x0, 0x0, 0x0);
    }
    $exec .= "\"";
    exec($exec);
}
function drag($startX, $startY, $endX, $endY, $event = 'event7')
{
    global $deviceName;

    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x1, 0x14a, 0x1);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x1);
    $exec .= "\"";
    exec($exec);
    $splitValue = 30;

    $intervalValueX = ($startX - $endX) / $splitValue;
    $intervalValueY = ($startY - $endY) / $splitValue;

    for ($i = 0; $i < $splitValue; $i++)
    {
        $exec = "adb -s {$deviceName} shell \"";
        $exec .= sendEvent($event, 0x3, 0x35, $startX - ($i * $intervalValueX));
        $exec .= sendEvent($event, 0x3, 0x36, $startY - ($i * $intervalValueY));
        $exec .= sendEvent($event, 0x0, 0x2, 0x0);
        $exec .= sendEvent($event, 0x0, 0x0, 0x0);
        $exec .= "\"";
        exec($exec);
    }
    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x1, 0x14a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x35, $endX);
    $exec .= sendEvent($event, 0x3, 0x36, $endY);
    $exec .= sendEvent($event, 0x0, 0x2, 0x0);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= "\"";
    exec($exec);
    echo 'drag ' . $startX . ', ' . $startY . ', ' . $endX . ', ' . $endY . PHP_EOL;
}
function drag_g2($startX, $startY, $endX, $endY, $event = 'event0')
{
    global $deviceName;

    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x3, 0x39, 0x155);
    $exec .= "\"";
    exec($exec);
    $splitValue = 30;

    $intervalValueX = ($startX - $endX) / $splitValue;
    $intervalValueY = ($startY - $endY) / $splitValue;

    for ($i = 0; $i < $splitValue; $i++)
    {
        $exec = "adb -s {$deviceName} shell \"";
        $exec .= sendEvent($event, 0x3, 0x35, $startX - ($i * $intervalValueX));
        $exec .= sendEvent($event, 0x3, 0x36, $startY - ($i * $intervalValueY));
        $exec .= sendEvent($event, 0x0, 0x0, 0x0);
        $exec .= "\"";
        exec($exec);
    }
    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x3, 0x35, $endX);
    $exec .= sendEvent($event, 0x3, 0x36, $endY);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= sendEvent($event, 0x3, 0x39, 0xffffffff);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= "\"";
    exec($exec);
    echo 'drag ' . $startX . ', ' . $startY . ', ' . $endX . ', ' . $endY . PHP_EOL;
}
function sendEvent($event, $one, $two, $three)
{
    return "sendevent /dev/input/{$event} {$one} {$two} {$three};";
}
